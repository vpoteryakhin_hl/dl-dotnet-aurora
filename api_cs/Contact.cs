﻿using System;

namespace HL.DataLake
{
    public class Contact
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Ext { get; set; }
    }
}
