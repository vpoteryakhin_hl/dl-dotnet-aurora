﻿using System;

namespace HL.DataLake
{
    public interface ITenantData
    {
        int GetContactId(string match_by, string json_keys);
        string GetEventJs(long event_id);
        long PostEventJs(string json_data);
    }

    public interface ITenantRepo
    {
        Tenant GetTenant(string tenant_name);
        ITenantData GetTenantData(string tenant_name);
    }
}
