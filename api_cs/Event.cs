﻿using System;

namespace HL.DataLake
{
    public class Event
    {
        public long id { get; set; }
        public DateTime event_date { get; set; }
        public int event_type { get; set; }
        public int contact_id { get; set; }
        public int campaign_id { get; set; }

        public int? asset_0 { get; set; }
        public int? asset_1 { get; set; }
        public int? asset_2 { get; set; }
        public int? asset_3 { get; set; }

        public int? int_data { get; set; }
        public long? source_id { get; set; }
        public string ext { get; set; }
    }
}
