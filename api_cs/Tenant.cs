﻿using System;

namespace HL.DataLake
{
    public class Tenant
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Data { get; set; }
        public string Dbcs { get; set; }
    }
}
