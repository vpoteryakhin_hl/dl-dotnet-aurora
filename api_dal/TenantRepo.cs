﻿using System;
using System.Collections.Generic;
using Npgsql;
using NpgsqlTypes;

namespace HL.DataLake
{
    public class TenantRepoAdb : ITenantRepo
    {
        private readonly IDictionary<string, Tenant> Tenants = new Dictionary<string, Tenant>();
        private string _tenant_dbkey;

        public string Dbcs { get; private set; }

        public TenantRepoAdb(string dbcs, string dbkey)
        {
            Dbcs = dbcs;
            _tenant_dbkey = ";Username=hlapi;Password=" + (string.IsNullOrEmpty(dbkey) ? "hlapi_test" : dbkey);
        }

        private Tenant GetTenantData(string tenant_name)
        {
            var tenant = new Tenant();
            using (var dbc = new NpgsqlConnection(Dbcs))
            {
                dbc.Open();
                var use_id = int.TryParse(tenant_name, out int tenant_id);
                var sql = "select tenant_id, tenant_name, tenant_dbcs, tenant_data from public.tenant where @a=tenant_" + (use_id ? "id" : "name");
                var cmd = new NpgsqlCommand(sql, dbc);
                cmd.Parameters.AddWithValue("a", use_id ? NpgsqlDbType.Integer : NpgsqlDbType.Varchar, use_id ? (object)tenant_id : (object)tenant_name);
                var reader = cmd.ExecuteReader();
                if (!reader.Read()) throw new ArgumentException("tenant name/id not found: " + tenant_name);
                tenant.Id = reader.GetInt32(0);
                tenant.Name = reader.GetString(1);
                tenant.Dbcs = reader.GetString(2);
                tenant.Data = !reader.IsDBNull(3) ? reader.GetString(3) : null;
                reader.Close();
            }
            // todo: make usr/pwd injected params
            if (!tenant.Dbcs.Contains("Password"))
                tenant.Dbcs += _tenant_dbkey;
            Tenants[tenant.Name] = tenant;
            Tenants[tenant.Id.ToString()] = tenant;
            return tenant;
        }

        public Tenant GetTenant(string tenant_name)
        {
            if (string.IsNullOrEmpty(tenant_name))
                throw new ArgumentNullException("tenant");
            if (Tenants.TryGetValue(tenant_name, out Tenant tenant))
                return tenant;
            return GetTenantData(tenant_name);
        }

        ITenantData ITenantRepo.GetTenantData(string tenant_name)
        {
            return new TenantDataAdb(GetTenant(tenant_name));
        }
    }
}
