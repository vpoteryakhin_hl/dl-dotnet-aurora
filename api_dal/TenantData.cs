﻿using System;
using System.Collections.Generic;
using Npgsql;
using NpgsqlTypes;

namespace HL.DataLake
{
    public class TenantDataAdb : ITenantData
    {
        public Tenant Tenant { get; private set; }

        public TenantDataAdb(Tenant tenant)
        {
            Tenant = tenant;
        }

        public int GetContactId(string match_by, string json_keys)
        {
            var contact_id = 0;
            using (var dbc = new NpgsqlConnection(Tenant.Dbcs))
            using (var cmd = new NpgsqlCommand("select api.get_contact_id(:tenant_id,:match_by,:keys)", dbc))
            {
                dbc.Open();
                cmd.Parameters.AddWithValue("tenant_id", NpgsqlDbType.Integer, Tenant.Id);
                cmd.Parameters.AddWithValue("match_by", NpgsqlDbType.Varchar, match_by);
                cmd.Parameters.AddWithValue("keys", NpgsqlDbType.Jsonb, json_keys);
                var reader = cmd.ExecuteReader();
                if (reader.Read())
                    contact_id = reader.GetInt32(0);
            }
            return contact_id;
        }

        public string GetEventJs(long event_id)
        {
            string json = null;
            using (var dbc = new NpgsqlConnection(Tenant.Dbcs))
                using (var cmd = new NpgsqlCommand("select api.get_event_js(:tenant_id,:event_id)", dbc))
                {
                    dbc.Open();
                    cmd.Parameters.AddWithValue("tenant_id", NpgsqlDbType.Integer, Tenant.Id);
                    cmd.Parameters.AddWithValue("event_id", NpgsqlDbType.Bigint, event_id);
                    var reader = cmd.ExecuteReader();
                    if (reader.Read())
                        json = reader.GetString(0);
                }
            return json;
        }

        public long PostEventJs(string json_data)
        {
            var event_id = 0L;
            using (var dbc = new NpgsqlConnection(Tenant.Dbcs))
            using (var cmd = new NpgsqlCommand("select api.post_event_js(:tenant_id,:jevent)", dbc))
            {
                dbc.Open();
                cmd.Parameters.AddWithValue("tenant_id", NpgsqlDbType.Integer, Tenant.Id);
                cmd.Parameters.AddWithValue("jevent", NpgsqlDbType.Jsonb, json_data);
                cmd.Prepare();
                var reader = cmd.ExecuteReader();
                if (reader.Read())
                    event_id = reader.GetInt64(0);
            }
            return event_id;
        }
    }
}
