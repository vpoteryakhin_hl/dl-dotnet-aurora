﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HL.DataLake;
using Microsoft.AspNetCore.Mvc;

namespace front_ws.Controllers
{
    [Route("api/[controller]")]
    public class EventsController : Controller
    {
        private readonly ITenantRepo _tenantRepo;

        public EventsController(ITenantRepo tenantRepo)
        {
            _tenantRepo = tenantRepo;
        }

        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "todo" };
        }

        [HttpGet("{tenant}/{id}")]
        public string Get(string tenant, int id)
        {
            var tdb = _tenantRepo.GetTenantData(tenant);
            Response.ContentType = "application/json";
            return tdb.GetEventJs(id);
        }
    
        [HttpPost("{tenant}")]
        public long Post(string tenant)
        {
            var tdb = _tenantRepo.GetTenantData(tenant);
            string json_data = null;
            if (Request.ContentType == "application/json" && Request.ContentLength > 3)
                using (var reader = new StreamReader(Request.Body, Encoding.UTF8))
                    json_data = reader.ReadToEnd();
            if (string.IsNullOrEmpty(json_data)) throw new ArgumentException("json body");
            return tdb.PostEventJs(json_data);
        }
    }
}
